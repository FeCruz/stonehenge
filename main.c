#include <windows.h>
#include <gl/gl.h>
#include <math.h>
#include <gl/glu.h>



LRESULT CALLBACK WindowProc(HWND, UINT, WPARAM, LPARAM);
void EnableOpenGL(HWND hwnd, HDC*, HGLRC*);
void DisableOpenGL(HWND, HDC, HGLRC);
void faceX(float yInit, float zInit, float yFinal, float zFinal, int steps, float normal, float x);
void faceY(float xInit, float zInit, float xFinal, float zFinal, int steps, float normal, float y);
void faceZ(float xInit, float yInit, float xFinal, float yFinal, int steps, float normal, float z);


int WINAPI WinMain(HINSTANCE hInstance,
                   HINSTANCE hPrevInstance,
                   LPSTR lpCmdLine,
                   int nCmdShow)
{
    WNDCLASSEX wcex;
    HWND hwnd;
    HDC hDC;
    HGLRC hRC;
    MSG msg;
    BOOL bQuit = FALSE;
    /* register window class */
    wcex.cbSize = sizeof(WNDCLASSEX);
    wcex.style = CS_OWNDC;
    wcex.lpfnWndProc = WindowProc;
    wcex.cbClsExtra = 0;
    wcex.cbWndExtra = 0;
    wcex.hInstance = hInstance;
    wcex.hIcon = LoadIcon(NULL, IDI_APPLICATION);
    wcex.hCursor = LoadCursor(NULL, IDC_ARROW);
    wcex.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
    wcex.lpszMenuName = NULL;
    wcex.lpszClassName = "GLSample";
    wcex.hIconSm = LoadIcon(NULL, IDI_APPLICATION);;


    if (!RegisterClassEx(&wcex))
        return 0;

    /* create main window */
    hwnd = CreateWindowEx(0,
                          "GLSample",
                          "OpenGL Sample",
                          WS_OVERLAPPEDWINDOW,
                          CW_USEDEFAULT,
                          CW_USEDEFAULT,
                          700,
                          700,
                          NULL,
                          NULL,
                          hInstance,
                          NULL);

    ShowWindow(hwnd, nCmdShow);

    /* enable OpenGL for the window */
    EnableOpenGL(hwnd, &hDC, &hRC);

    /* program main loop */
    while (!bQuit)
    {
        /* check for messages */
        if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
        {
            /* handle or dispatch messages */
            if (msg.message == WM_QUIT)
            {
                bQuit = TRUE;
            }
            else
            {
                TranslateMessage(&msg);
                DispatchMessage(&msg);
            }
        }
        else
        {
            void cubo()
            {
                glColor3f(0.5,0.5,0.5);
                faceX(-0.1, -0.1, 0.1, 0.1, 20, 1, 0.1);
                faceX(-0.1, -0.1, 0.1, 0.1, 20, -1, -0.1);

                glColor3f(0.25,0.25,0.25);
                faceY(-0.1, -0.1, 0.1, 0.1, 20, 1, 0.1);
                faceY(-0.1, -0.1, 0.1, 0.1, 20, -1, -0.1);

                glColor3f(0.75,0.75,0.75);
                faceZ(-0.1, -0.1, 0.1, 0.1, 20, 1, 0.1);
                faceZ(-0.1, -0.1, 0.1, 0.1, 20, -1, -0.1);
            }

            /* OpenGL animation code goes here */

            glClearColor(0.0,0.0,0.0,1);
            glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
            glEnable(GL_DEPTH_TEST);

            glMatrixMode(GL_PROJECTION);
            glLoadIdentity();
            glOrtho(-7.0, 4.0, -3.0, 3.0, -1000.0, 1000.0);
            gluLookAt(0.0,0.0, 0.25, -1.0, 0.0, 0.0, 0.0, 0.0, 1.0);
            glMatrixMode(GL_MODELVIEW);
            glLoadIdentity();
// Base
            int t;
            float yb = 1.65, yt = 1.65, xb = 0, xt = 1.65*6*M_PI/180.0;
            float base()
            {
                glPushMatrix();
                glScalef(2.0, 1.0, 4.0);
                cubo();
                glPopMatrix();
            }
            float topo()
            {
                glPushMatrix();
                glScalef(3.2, 1.0, 1.0);
                cubo();
                glPopMatrix();
            }
            float metade()
            {
                for(t = 0; t <= 180; t = t + 12)
                {
                    glPushMatrix();
                    glTranslatef(xb, yb, 0.4);
                    glRotatef ( t, 0, 0, 1 );
                    yb = yb - sin(t*M_PI/180.0)/1.65;
                    xb = xb - cos(t*M_PI/180.0)/1.65;
                    base();
                    glPopMatrix();

                    glPushMatrix();
                    glTranslatef(xt, yt, 0.9);
                    glRotatef ( t, 0, 0, 1 );
                    yt = yt - sin(t*M_PI/180.0)/1.65;
                    xt = xt - cos(t*M_PI/180.0)/1.65;
                    topo();
                    glPopMatrix();
                }
            }
            void Configura_Fonte_Pontual(float p[])
            {
                glEnable(GL_LIGHT0); // habilita o uso da fonte 0
                glEnable(GL_LIGHTING); // habilita o uso do modelo de ilumina��o

// apaga completamente a luz ambiente padr�o {0,0,0,1}
                float LAP[4] = {0.0, 0.0, 0.0, 1.0};
                glLightModelfv(GL_LIGHT_MODEL_AMBIENT, LAP);

// posi��o da fonte pontual
                glLightfv(GL_LIGHT0, GL_POSITION, p);

// defini��o das intensidades luminosas de cada componente
// neste exemplo n�o h� luz ambiente
                float la[4] = {0.0, 0.0, 0.0, 0.0};
                float ld[4] = {0.8, 0.8, 0.8, 1.0};
                float ls[4] = {0.2, 0.2, 0.2, 1.0};
                glLightfv(GL_LIGHT0, GL_AMBIENT, la);
                glLightfv(GL_LIGHT0, GL_DIFFUSE, ld);
                glLightfv(GL_LIGHT0, GL_SPECULAR, ls);

// defini��o das constantes de atenua��o atmosf�rica
                glLightf(GL_LIGHT0, GL_QUADRATIC_ATTENUATION, 0.1);
                glLightf(GL_LIGHT0, GL_LINEAR_ATTENUATION, 0.1);
                glLightf(GL_LIGHT0, GL_CONSTANT_ATTENUATION, 0.2);
            }
            void Desenha_Fonte_Pontual(float p[])
            {
                glDisable(GL_LIGHTING); // n�o ser� utilizado o modelo de ilumina��o
                glEnable(GL_POINT_SMOOTH);
                glPointSize(10);
                glColor3f(1.0, 1.0, 0.0);
                glBegin(GL_POINTS);

                glVertex3fv(p);
                glEnd();
            }
            void Configura_Material()
            {

// permite que a cor definida com glColor seja utilizada como material
                glEnable(GL_COLOR_MATERIAL);

// defini��o dos coeficientes de reflex�o do material
// nesse exemplo o material n�o emite luz (ke = 0)
                float ka[4] = {0.0, 0.0, 0.0, 1.0};
                float kd[4] = {0.8, 0.8, 0.8, 1.0};
                float ks[4] = {0.5, 0.4, 0.1, 1.0};
                float ke[4] = {0.0, 0.0, 0.0, 1.0};
                int n = 20;
                glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, ka);
                glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, kd);
                glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, ks);
                glMaterialfv(GL_FRONT_AND_BACK, GL_EMISSION, ke);
                glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, n);

            }
            void Desenha_Fonte_Refletora(float p[])
            {

// desenha um cone amarelo, com v�rtice em p, apontando para baixo e abertura 30�
                glDisable(GL_LIGHTING); // n�o ser� utilizado o modelo de ilumina��o
                glColor3f(1.0, 1.0, 0.0);

                glPushMatrix();

                glLoadIdentity();
                glTranslatef(p[0],p[1],p[2]); // muda referencial para sistema de coordenadas do objeto

// desenha a posi��o da fonte refletora
                glPointSize(5);
                glEnable(GL_POINT_SMOOTH);
                glBegin(GL_POINTS);

                glVertex3f(0.0, 0.0, 0.0);

                glEnd();

// desenha a dire��o da fonte refletora (0,0,-1)
                glBegin(GL_LINES);

                glVertex3f(0.0, 0.0, 0.0);
                glVertex3f(0.0, 0.0, -1.0);

                glEnd;

// desenha o cone da fonte refletora
                float u = 0;
                float r = 0.5*tan(30*M_PI/180); // altura 0.5 e �ngulo de 30�
                float du = 2*M_PI/10;
                while ( u < 2*M_PI )
                {

                    glBegin(GL_LINE_LOOP);

                    glVertex3f(0.0, 0.0,  0.0);
                    glVertex3f(r*cos(u), r*sin(u), -0.5);
                    glVertex3f(r*cos(u+du), r*sin(u+du), -0.5);

                    glEnd();
                    u = u + du;

                }

                glPopMatrix();

            }
            void Configura_Fonte_Refletora(float p[])
            {

                glEnable(GL_LIGHT1); // habilita o uso da fonte 0
                glEnable(GL_LIGHTING); // habilita o uso do modelo de ilumina��o

// apaga completamente a luz ambiente padr�o {0,0,0,1}
                float LAP[4] = {0.0, 0.0, 0.0, 1.0};
                glLightModelfv(GL_LIGHT_MODEL_AMBIENT, LAP);

                float d[3] = {0.0, 0.0, -1.0};              // Aten��o! S�o apena 3 valores
                glLightfv(GL_LIGHT1, GL_POSITION, p);       // posi��o da fonte
                glLightfv(GL_LIGHT1, GL_SPOT_DIRECTION, d); // dire��o da fonte
                glLightf(GL_LIGHT1, GL_SPOT_CUTOFF, 30);    // �ngulo da abertura do cone
                glLightf(GL_LIGHT1, GL_SPOT_EXPONENT, 1);   // fator de decaimento

// defini��o das intensidades luminosas de cada componente
// neste exemplo n�o h� luz ambiente
                float la[4] = {0.0, 0.0, 0.0, 0.0};
                float ld[4] = {0.8, 0.8, 0.8, 1.0};
                float ls[4] = {0.2, 0.2, 0.2, 1.0};
                glLightfv(GL_LIGHT1, GL_AMBIENT, la);
                glLightfv(GL_LIGHT1, GL_DIFFUSE, ld);
                glLightfv(GL_LIGHT1, GL_SPECULAR, ls);

// defini��o das constantes de atenua��o atmosf�rica
                glLightf(GL_LIGHT1, GL_QUADRATIC_ATTENUATION, 0.1);
                glLightf(GL_LIGHT1, GL_LINEAR_ATTENUATION, 0.1);
                glLightf(GL_LIGHT1, GL_CONSTANT_ATTENUATION, 0.2);

            }
            glEnable(GL_NORMALIZE);
            float p[] = {5, -6, 4, 1};
            float p1[] = {5, 2, 4, 1};
            Configura_Fonte_Pontual(p);
            Configura_Fonte_Refletora(p1);
            Configura_Material();
            Desenha_Fonte_Pontual(p);
            Desenha_Fonte_Refletora(p1);
            metade();
            glScalef( -1, -1, 1 );
            glTranslatef(0, 16.5/2, 0);
            metade();

            SwapBuffers(hDC);

            Sleep (100);
        }
    }

    /* shutdown OpenGL */
    DisableOpenGL(hwnd, hDC, hRC);

    /* destroy the window explicitly */
    DestroyWindow(hwnd);

    return msg.wParam;
}

LRESULT CALLBACK WindowProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
    switch (uMsg)
    {
    case WM_CLOSE:
        PostQuitMessage(0);
        break;

    case WM_DESTROY:
        return 0;

    case WM_KEYDOWN:
    {
        switch (wParam)
        {
        case VK_ESCAPE:
            PostQuitMessage(0);
            break;
        }
    }
    break;

    default:
        return DefWindowProc(hwnd, uMsg, wParam, lParam);
    }

    return 0;
}

void faceX(float yInit, float zInit, float yFinal, float zFinal, int steps, float normal, float x)
{
    float dy = (yFinal - yInit)/(float)steps;
    float dz = (zFinal - zInit)/(float)steps;
    float y;
    float z;


    for(y = yInit; y < yFinal; y+=dy)
    {
        for(z = zInit; z < zFinal; z+=dz)
        {
            glBegin(GL_QUADS);
            glNormal3f(normal, 0, 0);
            glVertex3f(x, y, z);
            glNormal3f(normal, 0, 0);
            glVertex3f(x, y+dy, z);
            glNormal3f(normal, 0, 0);
            glVertex3f(x, y+dy, z+dz);
            glNormal3f(normal, 0, 0);
            glVertex3f(x, y, z+dz);
            glEnd();
        }
    }
}

void faceY(float xInit, float zInit, float xFinal, float zFinal, int steps, float normal, float y)
{
    float dx = (xFinal - xInit)/(float)steps;
    float dz = (zFinal - zInit)/(float)steps;
    float x;
    float z;


    for(x = xInit; x < xFinal; x+=dx)
    {
        for(z = zInit; z < zFinal; z+=dz)
        {
            glBegin(GL_QUADS);
            glNormal3f(0, normal, 0);
            glVertex3f(x, y, z);
            glNormal3f(0, normal, 0);
            glVertex3f(x+dx, y, z);
            glNormal3f(0, normal, 0);
            glVertex3f(x+dx, y, z+dz);
            glNormal3f(0, normal, 0);
            glVertex3f(x, y, z+dz);
            glEnd();
        }
    }
}

void faceZ(float xInit, float yInit, float xFinal, float yFinal, int steps, float normal, float z)
{
    float dx = (xFinal - xInit)/(float)steps;
    float dy = (yFinal - yInit)/(float)steps;
    float x;
    float y;


    for(x = xInit; x < xFinal; x+=dx)
    {
        for(y = yInit; y < yFinal; y+=dy)
        {
            glBegin(GL_QUADS);
            glNormal3f(0, 0, normal);
            glVertex3f(x, y, z);
            glNormal3f(0, 0, normal);
            glVertex3f(x+dx, y, z);
            glNormal3f(0, 0, normal);
            glVertex3f(x+dx, y+dy, z);
            glNormal3f(0, 0, normal);
            glVertex3f(x, y+dy, z);
            glEnd();
        }
    }
}

void EnableOpenGL(HWND hwnd, HDC* hDC, HGLRC* hRC)
{
    PIXELFORMATDESCRIPTOR pfd;

    int iFormat;

    /* get the device context (DC) */
    *hDC = GetDC(hwnd);

    /* set the pixel format for the DC */
    ZeroMemory(&pfd, sizeof(pfd));

    pfd.nSize = sizeof(pfd);
    pfd.nVersion = 1;
    pfd.dwFlags = PFD_DRAW_TO_WINDOW |
                  PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
    pfd.iPixelType = PFD_TYPE_RGBA;
    pfd.cColorBits = 24;
    pfd.cDepthBits = 16;
    pfd.iLayerType = PFD_MAIN_PLANE;

    iFormat = ChoosePixelFormat(*hDC, &pfd);

    SetPixelFormat(*hDC, iFormat, &pfd);

    /* create and enable the render context (RC) */
    *hRC = wglCreateContext(*hDC);

    wglMakeCurrent(*hDC, *hRC);
}

void DisableOpenGL (HWND hwnd, HDC hDC, HGLRC hRC)
{
    wglMakeCurrent(NULL, NULL);
    wglDeleteContext(hRC);
    ReleaseDC(hwnd, hDC);
}
